Feature: ClassAtom

Scenario: ClassAtom Positive

Given Cat is a subclass of Mammal

Scenario: ClassAtom Negative

Given Cat is a subclass of not Dog
