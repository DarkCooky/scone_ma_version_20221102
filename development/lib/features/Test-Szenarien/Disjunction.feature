Feature: Disjunction

Scenario: Disjunction

Given Cat is a subclass of Dog or camel
Given Cat is a subclass of not Dog or camel
Given Cat is a subclass of not Dog or not camel
Given Cat is a subclass of Dog or camel or Bird

