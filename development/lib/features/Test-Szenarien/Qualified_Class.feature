Feature: Qualified Class

Scenario: some
Given a dog is defined as a mammal, who loves some food
Given a dog is defined as a mammal, which loves some food
Given a dog is defined as a mammal, that loves some food

Scenario: only
Given a dog is defined as a mammal, who loves only food
Given a dog is defined as a mammal, which loves only food
Given a dog is defined as a mammal, that loves only food

Scenario: is only
Given a dog is defined as a mammal, who is eating only food
Given a dog is defined as a mammal, which is eating only food
Given a dog is defined as a mammal, that is eating only food
Given a dog is defined as a mammal, who is not eating only food
Given a dog is defined as a mammal, which isn't eating only food

Scenario: does
Given a dog is defined as a mammal, who does eat only food
Given a dog is defined as a mammal, which does eat only food
Given a dog is defined as a mammal, that is does only food
Given a dog is defined as a mammal, who does not eat only food
Given a dog is defined as a mammal, which doesn't eat only food

Scenario: has
Given a dog is defined as a mammal, who has eaten only food
Given a dog is defined as a mammal, which has eaten only food
Given a dog is defined as a mammal, that has eaten only food
Given a dog is defined as a mammal, who has not eaten only food
Given a dog is defined as a mammal, which hasn't eaten only food

Scenario: has as
Given a dog is defined as a mammal, who has as ancestor only food
Given a dog is defined as a mammal, which has as ancestor only food
Given a dog is defined as a mammal, that has as ancestor only food
Given a dog is defined as a mammal, who has not as ancestor only food
Given a dog is defined as a mammal, which hasn't as ancestor only food

Scenario: has been
Given a dog is defined as a mammal, who has been eating only food
Given a dog is defined as a mammal, which has been eating only food
Given a dog is defined as a mammal, that has been eating only food
Given a dog is defined as a mammal, who has not been eating only food
Given a dog is defined as a mammal, which hasn't been eating only food

Scenario: is
Given a dog is defined as a mammal, who is green
Given a dog is defined as a mammal, which is red
Given a dog is defined as a mammal, that is blue

Scenario: is only preposition
Given a dog is defined as a mammal, who is eating_in only forest
Given a dog is defined as a mammal, which is eating_in only forest
Given a dog is defined as a mammal, that is eating_in only forest
Given a dog is defined as a mammal, who is not eating_in only forest
Given a dog is defined as a mammal, which isn't eating_in only forest

Scenario: is only _preposition
Given a dog is defined as a mammal, who is eating in only forest
Given a dog is defined as a mammal, which is eating in only forest
Given a dog is defined as a mammal, that is eating in only forest
Given a dog is defined as a mammal, who is not eating in only forest
Given a dog is defined as a mammal, which isn't eating in only forest
