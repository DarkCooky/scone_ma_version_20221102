Feature: Conjunction
 
Scenario: Conjunction Positive

Given Cat is a subclass of Dog and camel
Given Cat is a subclass of Dog and camel and Bird

Scenario: Conjunction Negative

Given Cat is a subclass of neither Dog nor camel
Given Cat is a subclass of neither Dog nor camel nor Bird
Given Cat is a subclass of collie