Feature: Qualified Individual

Scenario: some
Given Copo loves some food


Scenario: only
Given Copo loves only food

Scenario: is only
Given Copo is eating only food
Given Copo is not eating only food
Given Copo isn't eating only food

Scenario: does
Given Copo does eat only food
Given Copo does not eat only food
Given Copo doesn't eat only food

Scenario: has
Given Copo has eaten only food
Given Copo has not eaten only food
Given Copo hasn't eaten only food

Scenario: has as
Given Copo has as ancestor only food
Given Copo has not as ancestor only food
Given Copo hasn't as ancestor only food

Scenario: has been
Given Copo has been eating only food
Given Copo has not been eating only food
Given Copo hasn't been eating only food


Scenario: is only preposition
Given Copo is eating_in only forest
Given Copo is not eating_in only forest
Given Copo isn't eating_in only forest

Scenario: is only _preposition
Given Copo is eating in only forest
Given Copo is not eating in only forest
Given Copo isn't eating in only forest
