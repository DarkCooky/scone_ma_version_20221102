#!/usr/bin/ruby

require 'logger'
require 'open3'
require 'io/wait'
require_relative './hetsUtils.rb'

#Read the command line arguments
#The first argument is the filename
if ARGV[0]=="-h"||ARGV[0]=="--help" then
	$logger.debug "Scone.rb help : to run this script, run the command :\n\truby scone.rb [path_to_dol_file] \"[consistency_checks_nodes_names]\" \"[theorems_to_prove_nodes_names]\"\nNode names must be separated by the caracter ';'. All the arguments must be present. If you don't want to do consistency checks for exemple, use an empty string : \"\"."
	exit
else
	if ARGV[0]=="" then
		$logger.debug "Error : missing file name. Run \"ruby scone.rb --help\" for more informations"
		exit
	end
	if ARGV.length < 3 then
		$logger.debug "Error : missing argument(s). Run \"ruby scone.rb --help\" for more informations"
		exit
	end
end

inputfilename = ARGV[0]
#The second argument is the list of the nodes that are consistency checks
consistency_checks = ARGV[1].split(";")
#The third argument is the list of the nodes that are theorem to be proved
proof_checks = ARGV[2].split(";")

hetsUtils = HetsUtils.new(inputfilename, consistency_checks,proof_checks)
terms = Hash.new
terms["Human"] = "Class"
terms["Mary"] = "Individual"
terms["Jean-Pierre"] = "Individual"
#hetsUtils.check_signature(terms,"Scenario")
hetsUtils.execute()

