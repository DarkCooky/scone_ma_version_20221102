class FeatureDictionary

  attr_accessor :refTerms, :newTerms

  def initialize
    @refTerms = Hash.new
    @newTerms = Hash.new
  end

  def to_s
    String s = "\n \n"
    @refTerms.each{ | k,v|
      s = s + k.to_s + " => \n" + v.to_s + "\n \n"
    }
    @newTerms.each{ | k,v|
      s = s + k.to_s + " => \n" + v.to_s + "\n \n"
    }
    s
  end
end
