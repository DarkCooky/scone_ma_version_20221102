require_relative 'canonical'
require 'logger'


class SconeParseTransform < Parslet::Transform

  class Helper

    def initialize
      @canonical = Canonical.new
    end

    def generateTerm(name,refTerms,newTerms,type, visitor)
      key = @canonical.canonize(name.to_s)
      $logger.debug("refTerms are : " +  refTerms.to_s)
      $logger.debug("newTerms are : " +  newTerms.to_s)
      $logger.debug   "looking up for #{key}..."
      value = nil

      refTerms.each{ |k,ont|
              if ont.has_key?(key) then
                value = ont[key][:name]
                $logger.debug "ref term found : #{ont[key]}"
                visitor.incrementCount(ont,key)
              end
      }

      if(value == nil)
        if newTerms.has_key?(key)
          $logger.debug "new term already created"
          term = newTerms[key]

          if visitor.type == :CQ && term[:type] == "Class"
            term[:origin] = :CQ
          end
          value = term[:name]
        else
          value = name
          newTerms[key] = { name: value , type: "#{type}", origin: visitor.type}
          $logger.debug "new term found : generating a new entry : #{newTerms[key]}"
        end
      end

      value
    end
  end


  #We can improve that class and maybe put it in its own module
  class NameGenerator  
  
	  def initialize
	  	  @counter = 0
	  end

	  def getNewIndivName(c1 , c2,sign)
	  	neg = if sign then "" else "not_" end
	  	newName = "#{c1}_#{neg}#{c2}_#{@counter}"
	  	@counter = @counter + 1
	  	newName
	  end

  end

  nameGenerator = NameGenerator.new
  helper = Helper.new

  ###################### terms ######################

  rule( :class_name => simple(:name)) { 
    helper.generateTerm(name,refTerms,newTerms,"Class", occurenceCounterVisitor) 
  }

  rule( :pos_class => simple(:class_name)) { class_name }
  rule( :negation => simple(:negation) , :pos_class => simple(:class_name) ) { "not #{class_name}" }
  rule( :class_atom => simple(:class_atom)) { class_atom }
  rule( :negation => simple(:negation) , :predicate_name => simple(:predicate_name)) { "not #{predicate_name}" }


  rule( :predicate_name => simple(:predicate_name)) {
    helper.generateTerm(predicate_name,refTerms,newTerms,"ObjectProperty", occurenceCounterVisitor)
  }
  
  rule( :predicate_name => simple(:predicate_name) , :preposition => simple(:preposition)) {
   	helper.generateTerm("#{predicate_name}_#{preposition}",refTerms,newTerms,"ObjectProperty", occurenceCounterVisitor)
  }
  rule( :negation => simple(:negation) , :predicate_name => simple(:predicate_name) , :preposition => simple(:preposition)) {
   	"not #{predicate_name}_#{preposition}"
  }


  rule( :qualified_class => simple(:c) ) { c }



  rule( :predicate  => simple(:p) , :quantifier => simple(:q) , :class_expression => simple(:c)) {"#{p} #{q} #{c}"}

  rule(:predicate => simple(:p)) { p }
  
  rule( :quantifier => simple(:quantifier)) { quantifier }

  rule( :pos_class => simple(:c) , :qualifier => simple(:q)) { "( #{c} and #{q} )" }


  rule( :disjunction => sequence(:c)) { "( " + c.inject { |list, elt| "#{list} or #{elt}" } + " )" }
  rule( :conjunction => sequence(:c)) { "( " + c.inject { |list, elt| "#{list} and #{elt}" } + " )" }

  rule( :class_expression => simple(:class_expression)) { class_expression }

  rule( :qualified_individual => simple(:i) ) { i }

  rule( :indiv_name => simple(:i), :predicate => simple(:p), :quantifier => simple(:q), :class_expression => simple(:exp ) ) {
    "Individual: #{i} Types: ( #{helper.generateTerm(i + "_Class",refTerms,newTerms,"Class", occurenceCounterVisitor)} and #{p} #{q} #{exp} )" 
  }

  ###################### sentences ######################
  
  ######################  Definitions and propostions

  #foo is defined as bar
  rule( :pos_class => simple(:c) , :defined => simple(:d)) { "Class: #{c} EquivalentTo: #{d}" }
  
  #Doobie_Brothers is enumerated as John and Bob
  rule( :pos_class => simple(:c) , :enumerated => sequence(:enum)) {
   "Class: #{c} EquivalentTo: { " + enum.inject { |list, elt| "#{list} , #{elt}" } + " }" }

  #every dog is a mammal
  rule( :every => simple(:every) , :class_name => simple(:c) , :class_expression => simple(:exp)) {
  	"Class: #{c} SubClassOf: #{exp}"  }

  #all dogs are a mammals
  rule( :all => simple(:all) , :class_name => simple(:c) , :class_expression => simple(:exp)) {
  	"Class: #{c} SubClassOf: #{exp}"  }

  #dog is a subclass of mammal
  rule( :class_name => simple(:c) , :subclass_of => simple(:subclass_of) , :class_expression => simple(:exp)) {
  	"Class: #{c} SubClassOf: #{exp}"  }

  #no cat is a dog
  rule(:negation => simple(:neg) , :class_name => simple(:c), :class_expression => simple(:exp)) {
  	"Class: #{c} DisjointWith: #{exp}"}

  #Dog and Cat and Bird are disjoint
  rule(:disjoint => sequence(:disjoint)) {
  	"DisjointClasses: " +disjoint.inject { |list, elt| "#{list} , #{elt}" } }

  #some dogs are cute
  rule( :some => simple(:some) , :class_name => simple(:c) , :class_expression => simple(:exp)) {
  	"Individual: #{nameGenerator.getNewIndivName(c,exp,true)} Types: #{c} , #{exp}"  }
  
  #some dogs aren't cute
  rule( :some => simple(:some) , :class_name => simple(:c) , 
  		:negation => simple(:negation) , :class_expression => simple(:exp)) { 
  	"Individual: #{nameGenerator.getNewIndivName(c,exp,false)} Types: #{c} , Not #{exp}"  }

  #dogs can be cute
  rule( :class_name => simple(:c) , :canbe => simple(:canbe),  :class_expression => simple(:exp)) {
  	"Individual: #{nameGenerator.getNewIndivName(c,exp,true)} Types: #{c} , #{exp}"  }

  ######################  FACTS

  #John is an idiot
  rule( :indiv_name => simple(:i) , :class_expression => simple(:y)) { "Individual: #{i} Types: #{y}" }

  #John is not an idiot
  rule( :indiv_name => simple(:i) , :negation => simple(:negation) , :class_expression => simple(:y)) {
  	"Individual: #{i} Types: not #{y}" }

  #John doesn't love Jane
  rule( :indiv1 => simple(:i1) , :predicate => simple(:p), :indiv2 => simple(:i2)) { "Individual: #{i1} Facts: #{p} #{i2} " }

  #John is the same as Jill
  rule( :indiv1 => simple(:i1) , :indiv2 => simple(:i2)) { "SameIndividual: #{i1} , #{i2} " }

  #John is not the same as Jill
  rule( :indiv1 => simple(:i1), :negation => simple(:negation), :indiv2 => simple(:i2)) { "DifferentIndividuals: #{i1} , #{i2} " }

  #Jill and Mary and Robert are different
  rule(:different => sequence(:i)) { "DifferentIndividuals: " + i.inject { |list, elt| "#{list} , #{elt}" } } 

  # %OWL
  # """
  #   Class: Nut 
  #   Class: Peanut 
  #       SubClassOf: Nut   
  # """

  ######################  Leaf
  rule( :indiv_name => simple(:i)) {
   helper.generateTerm(i,refTerms,newTerms,"Individual", occurenceCounterVisitor) 
  }

  ######################  Intermediary nodes
  rule( :universal_negative => simple(:u)) { u }
  rule( :universal_positive => simple(:u)) { u }
  rule( :particular => simple(:particular)) { particular }
  rule( :instance => simple(:i)) { i }
  rule( :relation => simple(:r)) { r }
  rule( :equation => simple(:e)) { e }
  rule( :proposition => simple(:p)) { p }
  rule( :fact => simple(:f)) { f }
  rule( :source => simple(:source)) { source }
  rule( :definition => simple(:d)) { d }
  rule( :sentence => simple(:s)) { s }

  ######################  Document root
  rule( :text => sequence(:c)) { c.inject { |text, sentence| "#{text} \r\n #{sentence}" } }
  
end