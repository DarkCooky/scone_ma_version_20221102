class SconeParser < Parslet::Parser
  root(:text)

  rule(:text) { sentence.repeat().as(:text) }
  
  rule(:infer_sentence) { (definition | universal | fact | source ).as(:infer_sentence) >> space.maybe >> eol }

  rule(:sentence) { (definition | proposition | fact | source ).as(:sentence) >> space.maybe >> dot.maybe >> eol  }


  rule(:source) { space? >> language_ID >> delimiter >> source_body >> delimiter  }


### Class-Expression

  rule(:definition) { (pos_class >>  ( is | are ) >> ( define | enumerate )).as(:definition) }

  rule(:enumerate) { (enumerated >> as >> indiv_list).as(:enumerated) }

  rule(:define) { (defined >> as >> class_expression).as(:defined) }

  rule(:proposition){ (universal | particular).as(:proposition) }

  rule(:universal) { universal_positive | universal_negative }

  rule(:universal_positive){
    ( ( every.as(:every) >> class_name >> is >> class_expression ) |
      ( class_name >> ( is | are ) >> a_subclass_of.as(:subclass_of) >> class_expression) |
      ( all.as(:all) >> class_name >> are >> class_expression )
    ).as(:universal_positive)
  }

  rule(:universal_negative){
    ( ( no.as(:negation) >> class_name >> is >> class_expression ) |
      ( (class_name >> ( _and >> class_name).repeat(1,nil)
        ).as(:disjoint) >> are >> disjoint )).as(:universal_negative)
  }

  rule(:particular) { particular1 | particular2 }

  rule(:particular1){
    ( some.as(:some) >> space >> class_name >> (are | arenot) >>
      class_expression
    ).as(:particular)
  }

  rule(:particular2){
    (  class_name >> canbe.as(:canbe) >> class_expression).as(:particular)
  }


 ### Individual Sentences

    rule(:instance) { (qualified_individual.as(:qualified_individual)  |  indiv_atom >> ( isnot | is ) >> class_expression ).as(:instance) 
                        }

    rule(:relation) { ( indiv_atom.as(:indiv1) >> predicate_phrase >> indiv_name.as(:indiv2) ).as(:relation) }

    rule(:different) { ( indiv_list >> are >> different_KW ).as(:different) }

    rule(:equation) { ( indiv_atom.as(:indiv1) >> ( isnot | is ) >> the_same_as >> indiv_name.as(:indiv2) ).as(:equation) }

    rule(:fact) { (equation | different | relation | instance).as(:fact) }


  ### Class-Expression

    rule(:class_expression) { ( conjunction | disjunction | qualified_class | class_atom ).as(:class_expression) }

    rule(:pos_class) { (an.maybe >> class_name).as(:pos_class) }

    rule(:class_atom) { ( _not.maybe >> pos_class ).as(:class_atom) }

    rule(:conjunction) { (conjunction_and | conjunction_neither).as(:conjunction) }

    rule(:conjunction_and) { pos_class >> ( _and >> class_atom ).repeat(1,nil) }

    rule(:conjunction_neither) { neither.as(:negation) >> pos_class >> ( nor.as(:negation) >> pos_class ).repeat(1,nil) }

    rule(:disjunction) { ( class_atom >> ( _or >> class_atom ).repeat(1,nil) ).as(:disjunction) }

    rule(:qualified_class) { ( pos_class >> comma.maybe >> pronoun >> qualifier).as(:qualified_class) }

    rule(:qualifier) { ((predicate_phrase_plural >> quantifier >> class_expression ) | 
                        ( ( is | are ) >> class_expression )).as(:qualifier) }



  ### Other

    rule(:quantifier) { ( some | only ).as(:quantifier) >> space }

    rule(:predicate_phrase) { (indiv_predicate_open.maybe >> predicate_name >> preposition.maybe).as(:predicate) }

    rule(:predicate_phrase_plural) { (predicate_open_plural.maybe >> predicate_name >> preposition.maybe).as(:predicate) }

    rule(:indiv_predicate_open) { doesnot | does | hasnot | has | ( isnot | is ) >> ( a | an | the ).maybe  }

    rule(:predicate_open_plural) { indiv_predicate_open | arenot | are  }

    rule(:preposition) { ( str('of') | str('than') | str('to') | str('on') | str('in') ).as(:preposition) >> space }

    rule(:qualified_individual) { indiv_name >> predicate_phrase >> quantifier >> class_expression }

    rule(:indiv_atom) { thereis | indiv_name }

    rule(:indiv_list) { (indiv_name >>  ( _and >> indiv_name).repeat(1,nil) ) }




  ###################### Words / Word Structure ######################
  exclusions = /And|Then|Given|some|only/

  rule(:word) { match(/(?!#{exclusions})[\w'’]/).repeat(1)  }

  rule(:class_name) { word.as(:class_name) >> space? }
  rule(:predicate_name) { word.as(:predicate_name) >> space? }
  rule(:indiv_name) { word.as(:indiv_name)  >> space? }

  rule(:space)  { match('[\s]').repeat(1) }
  rule(:space?) { space.maybe }
  rule(:crlf) { match('[\r\n]').repeat(1) }
  rule(:line_end) { crlf >> space? }
  rule(:eol) { line_end.repeat }

  rule(:thereis) { str('there') >> space >> is >> str('some') >> space >> indiv_name >> comma >> that }
  rule(:that) { str('that') >> space }
  rule(:_and) { str('and') >> space }
  rule(:nor) { str('nor') >> space }
  rule(:neither) { str('neither') >> space }
  rule(:_or) { str('or') >> space }
  rule(:comma) { str(',').maybe >> space? }
  rule(:the) { str('the') >> space }
  rule(:an) { str('a') >> str('n').maybe  >> space }
  rule(:as) { str('as') >> space }
  rule(:been) { str('been') >> space }
  rule(:pronoun) { ( str('who') | str('which') | str('that') ) >> space }
  rule(:some) { str('some') }
  rule(:only) { str('only') }

  rule(:does) { str('does') >> space }
  rule(:has) { str('has') >> space >> ( as | been ).maybe }
  rule(:is) { str('is') >> space }
  rule(:canbe) { str('can') >> space >> str('be') >> space}

  rule(:_not) { str('not').as(:negation) >> space }
  rule(:doesnot) { ( str('doesn’t') | str('doesn\'t') | str('does not') ).as(:negation) >> space }
  rule(:hasnot) { ( str('hasn’t') | str('hasn\'t') | str('has not') ).as(:negation) >> space >> ( as | been ).maybe  }
  rule(:isnot) { ( str('isn’t') | str('isn\'t') | str('is not') ).as(:negation) >> space }

  rule(:dot) { str(".") >> space? }
  rule(:delimiter) {  eol >> str('"""') >> space? >> eol }
  rule(:a) { str('a') >> space }
  rule(:no) { str('no') >> space }
  rule(:are) {str('are') >> space }
  rule(:arenot) {
    (str('aren’t') | str('aren\'t') | str('are not')).as(:negation) >> space
  }
  rule(:same_KW) {str('same') >> space }
  rule(:different_KW) {
    # optional space because it can be at the end of line
    str('different') >> space?
  }
  rule(:defined) { str('defined') >> space }
  rule(:enumerated) { str('enumerated') >> space }
  rule(:disjoint) { str('disjoint') >> space? }
  rule(:every) { str('every') >> space }
  rule(:all) { str('all') >> space }
  rule(:subclass) { str('subclass') >> space? }
  rule(:of) { str('of') >> space }
  rule(:a_subclass_of) { a >> subclass >> of }
  rule(:the_same_as) {
    # Caters for indeterminate amount of white space inside the expression.
    the >> same_KW >> as
  }

  rule(:language_ID) { ( str("%CommonLogic") | str("%OWL") ) >> space? }
  rule(:source_body) {
    ( ( str('"').absent? >> str('"').absent? >> str('"').absent? >> any
      ).repeat().as(:source) ) >> space?
  }

end
