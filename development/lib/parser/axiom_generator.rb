class AxiomGenerator
  generateAxiom(term)
    "#{term.type} : #{term.name}"
end
