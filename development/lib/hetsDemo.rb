#!/usr/bin/ruby

require 'open3'
require 'io/wait'
require 'logger'
require_relative './hetsUtils.rb'

filename = "/vagrant/project/resources/DOLFiles/localmanuallyGeneratedTranslation.dol"
hetsUtils = HetsUtils.new

dict = hetsUtils.getDictionnary("file:///vagrant/project/resources/OWLFiles/cycle.owl")
$logger.debug "Dictionnary generated : #{dict}"

prefixes = hetsUtils.getPrefixes("file:///vagrant/project/resources/DOLFiles/ont2.owl")
$logger.debug "Prefixes : #{prefixes}"

#New way
consistency_checks = {"CQ_1_Scenario_1"=>true,"CQ_2_Scenario_1"=>true,"CQ_3_Scenario_1"=>false,"CQ_1_Scenario_2"=>true,"CQ_2_Scenario_2"=>true,"CQ_3_Scenario_2"=>false}
resConsistency = hetsUtils.checkNodesConsistency(filename,consistency_checks)
theorems = {"F1_Scenario_1"=>true,"F2_Scenario_1"=>true,"F3_Scenario_1"=>true,"F4_Scenario_1"=>true,"F1_Scenario_2"=>true,"F2_Scenario_2"=>true,"F3_Scenario_2"=>true}
resProve = hetsUtils.proveNodes(filename,theorems)
$logger.debug "Result of the consistency checks : #{resConsistency}"
$logger.debug "Result of the theorems prove : #{resProve}"

#Old way
consistency_checks = Array.new
consistency_checks.insert(6,"CQ_1_Scenario_1","CQ_2_Scenario_1","CQ_3_Scenario_1","CQ_1_Scenario_2","CQ_2_Scenario_2","CQ_3_Scenario_2")
theorems = Array.new
theorems.insert(7,"F1_Scenario_1","F2_Scenario_1","F3_Scenario_1","F4_Scenario_1","F1_Scenario_2","F2_Scenario_2","F3_Scenario_2")

resConsistency,resProve = hetsUtils.execute(filename, consistency_checks.compact,theorems.compact)
$logger.debug "Result of the consistency checks : #{resConsistency}"
$logger.debug "Result of the theorems prove : #{resProve}"



=begin
terms = Hash.new
terms["Male"] = "Class"
terms["Mother"] = "Class"
terms["Person"] = "Class"
terms["fatherOf"] = "ObjectProperty"
terms["GrandParent_OF"] = "ObjectProperty"
terms["mother of"] = "ObjectProperty"
terms["older_than"] = "ObjectProperty"
terms["Parent Of"] = "ObjectProperty"
#terms["Class"] = "Male,Mother,Person,Female"
#terms["ObjectProperty"] = "fatherOf,GrandParent_OF,mother of,older_than,Parent Of,loverOf"
#Uncomment this one if you want the test to fail
#resSignature = hetsUtils.check_signature("combinedImportsScenario_1",terms)
#$logger.debug resSignature
=end