

require 'logger'
require 'set'
require 'pp'

# $logger.debug "signature loaded"


###### some constants

CLASS = "owl:Class"
ROLE  = "owl:ObjectProperty"
DATA  = "owl:DatatypeProperty"
INDIVIDUAL = "owl:NamedIndividual"


###### Signatures

class Signature 

 attr_accessor :classes, 
               :properties,
               :individuals

 def initialize()
    #if cSet.kind_of?(Set) & pSet.kind_of?(Set)  & iSet.kind_of?(Set)
      @classes = [].to_set
      @properties = [].to_set
      @individuals = [].to_set
  end
  
  def addClass(name)
    @classes << name 
  end

  def addProperty(name)
    @properties << name 
  end

  def addIndividual(name)
    @individuals << name 
  end

  def show 
    $logger.debug 
    $logger.debug "$*** Classes ***"    
    if @classes.empty? == false 
      @classes.each do |x|
        $logger.debug x
      end
    end 

    $logger.debug 
    $logger.debug "*** Properties ***"
    if properties.empty? == false 
      @properties.each do |x|
        $logger.debug x
      end
    end
    
    $logger.debug 
    $logger.debug "*** Individuals ***"
    if individuals.empty? == false
       @individuals.each do |x|
         $logger.debug x
       end
    end
    $logger.debug 
  end 
end


#top = Symbols.new(CLASS, "Thing")
#top.show

#err = Symbols.new(3, "a") throws an error

# testS = Set.new [1,2]


# testSig = Signature.new()

# testSig.addProperty("loves")
# testSig.addClass("Human")
# testSig.addClass("Mammal")
# testSig.addIndividual("joe")


# testSig.show