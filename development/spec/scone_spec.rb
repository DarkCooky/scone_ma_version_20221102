require_relative 'spec_helper'
require_relative '../lib/parser/canonical'

describe Canonical do

	before :each do
	      @c = Object.new
  		  @c.extend(Canonical)
	end

  it "should do nothing" do
    expect(true).to eq(true)
  end

  describe "#canonize" do
    it "canonize all strings!" do
        result = @c.canonize('is_Father_of')
        expect(result).to eq('isfatherof')
    end
end
end