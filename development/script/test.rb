#!/usr/bin/env ruby

  require 'parslet'
  require 'pp'
  require 'solid_assert'
  require 'parslet/convenience'

	SolidAssert.enable_assertions


    class SconeParser < Parslet::Parser

    ###################### literals ######################
    rule(:space)  { match('[\s]').repeat(1) }
    rule(:space?) { space.maybe }
    rule(:crlf) { match('[\r\n]').repeat(1) }
    rule(:line_end) { crlf >> space? }
    rule(:eol) { line_end.repeat }

	rule(:_and) { str('and') >> space }
    rule(:nor) { str('nor') >> space }
    rule(:neither) { str('neither') >> space }	
    rule(:_or) { str('or') >> space }
    rule(:comma) { str(',').maybe >> space? }
    rule(:the) { str('the') >> space }
    rule(:an) { str('a') >> str('n').maybe  >> space }
    rule(:as) { str('as') >> space }
    rule(:pronoun) { ( str('who') | str('which') | str('that') ) >> space }
    rule(:some) { str('some') }
    rule(:only) { str('only') }

    rule(:does) { str('does') >> space }
    rule(:has) { str('has') >> space }
    rule(:is) { str('is') >> space }

    rule(:_not) { str('not').as(:negation) >> space }
    rule(:doesnot) { ( str('doesn’t') | str('doesn\'t') | str('does not') ).as(:negation) >> space }
    rule(:hasnot) { ( str('hasn’t') | str('hasn\'t') | str('has not') ).as(:negation) >> space }
    rule(:isnot) { ( str('isn’t') | str('isn\'t') | str('is not') ).as(:negation) >> space }

	rule(:quantifier) { ( some | only ).as(:quantifier) >> space }

	###################### terms ######################
	exclusions = /every|some|no|infer|that/

    rule(:word) { match(/(?!#{exclusions})[\w'’]/).repeat(1)  }

   	rule(:class_name) { word.as(:class_name) >> space? }
   	rule(:predicate_name) { word.as(:predicate_name) >> space? }
   	rule(:predicate_fragment) { word.as(:predicate_fragment) >> space? }

   	rule(:pos_class) { (an.maybe >> class_name).as(:pos_class) }
   	rule(:class_atom) { ( _not.maybe >> pos_class ).as(:class_atom) }
    
    rule(:predicate_end) { ( str('of') | str('than') | str('to') | str('on') | str('in') ).as(:predicate_end) >> space }
    rule(:predicate_open) { doesnot | does | ( hasnot | has ) >> as }
    rule(:predicate_1) { predicate_open >> predicate_name }
    rule(:predicate_2) { ( isnot | is ) >>  ( the | an ).maybe >> predicate_fragment >> predicate_end }
    rule(:predicate) { (predicate_1 | predicate_2).as(:predicate) }

    rule(:disjunction) { ( class_atom >> ( _or >> class_atom ).repeat(1,nil) ).as(:disjunction)  }

    rule(:conjunction_and) { pos_class >> ( _and >> class_atom ).repeat(1,nil) }
    rule(:conjunction_neither) { neither.as(:negation) >> pos_class >> ( nor.as(:negation) >> pos_class ).repeat(1,nil) }
    rule(:conjunction) { (conjunction_and | conjunction_neither).as(:conjunction) }

    rule(:qualifier) { (( predicate >> quantifier >> class_expression ) | ( is >> class_expression )).as(:qualifier) }
    rule(:qualified_class) { ( pos_class >> comma.maybe >> pronoun >> qualifier ).as(:qualified_class) }

    rule(:class_expression) { 
    	(
    						conjunction |
							disjunction |
							qualified_class |
    						class_atom 
    	).as(:class_expression)
     }

    ###################### sentences ######################
    rule(:dot) { str(".") >> space? }
    rule(:delimiter) {  eol >> str('"""') >> space? >> eol }
    rule(:a) { str('a') >> space }
    rule(:no) { str('no') >> space }
	rule(:are) {str('are') >> space }
	rule(:arenot) { ( str('aren’t') | str('aren\'t') | str('are not') ).as(:negation) >> space }
	rule(:same_KW) {str('same') >> space }
	rule(:different_KW) {str('different') >> space?} #optional space because it can be at the end of line
	rule(:defined) { str('defined') >> space }
	rule(:enumerated) { str('enumerated') >> space }
	rule(:disjoint) { str('disjoint') >> space? }
	rule(:every) { str('every') >> space }
	rule(:subclass) { str('subclass') >> space? }
	rule(:of) { str('of') >> space }
	rule(:a_subclass_of) { a >> subclass >> of }
	rule(:the_same_as) { the >> same_KW >> as } # to cater for indeterminate amount of white spaces inside the expression.
    rule(:indiv_name) { word.as(:indiv_name)  >> space? }
    rule(:language_ID) { ( str("%CommonLogic") | str("%OWL") ) >> space? }
	rule(:source_body) { ( ( str('"').absent? >> str('"').absent? >> str('"').absent? >> any).repeat().as(:source) ) >> space? }

	rule(:indiv_list) { (indiv_name >>  ( _and >> indiv_name).repeat(1,nil) ) }

	rule(:instance) { ( indiv_name  >> ( isnot | is ) >> class_expression ).as(:instance) }
	rule(:relation) { ( indiv_name.as(:indiv1) >> predicate >> indiv_name.as(:indiv2) ).as(:relation) }
	rule(:different) { ( indiv_list >> are >> different_KW ).as(:different) }
	rule(:equation) { ( indiv_name.as(:indiv1) >> ( isnot | is ) >> the_same_as >> indiv_name.as(:indiv2) ).as(:equation) }
	rule(:fact) { (equation | relation | different | instance).as(:fact) }

	rule(:enumerate) { (enumerated >> as >> indiv_list).as(:enumerated) }
	rule(:define) { (defined >> as >> class_expression).as(:defined) }
	rule(:definition) { (class_name >> is >> ( define | enumerate )).as(:definition) }

	rule(:universal_negative){ (
								( no.as(:negation) >> class_name >> is >> class_expression ) |
								( (class_name >> ( _and >> class_name).repeat(1,nil)).as(:disjoint) >> are >> disjoint )
							   ).as(:universal_negative) }
	rule(:universal_positive){ (
								( every.as(:every) >> class_name >> is >> class_expression ) |
								( class_name >> is >> a_subclass_of.as(:subclass_of) >> class_expression )
							   ).as(:universal_positive) }
	rule(:universal) { universal_positive | universal_negative }
	rule(:particular){ ( some.as(:some) >> space >> class_name >> (are | arenot) >> class_expression ).as(:particular) }
	rule(:proposition){ (universal | particular).as(:proposition) }
	rule(:source) { space? >> language_ID >> delimiter >> source_body >> delimiter  }

	rule(:infer_sentence) { (definition | universal | fact | source ).as(:infer_sentence) >> space.maybe >> eol }

	rule(:sentence) { (definition | proposition | fact | source ).as(:sentence) >> space.maybe >> dot.maybe >> eol  }

	rule(:text) { sentence.repeat().as(:text) }

    root(:text)
    
  end

class SconeParseTransform < Parslet::Transform

  #We can improve that class and maybe put it in its own module
  class NameGenerator  
  
	  def initialize
	  	  @counter = 0
	  end

	  def getNewIndivName(c1 , c2,sign)
	  	neg = if sign then "" else "not_" end
	  	newName = "#{c1}_#{neg}#{c2}_#{@counter}"
	  	@counter = @counter + 1
	  	newName
	  end
  
  end

  nameGenerator = NameGenerator.new

  ###################### terms ######################

  rule( :class_name => simple(:name)) { name.to_s.upcase  }
  rule( :pos_class => simple(:class_name)) { class_name }
  rule( :negation => simple(:negation) , :pos_class => simple(:class_name) ) { "not #{class_name}" }
  rule( :class_atom => simple(:class_atom)) { class_atom }
  rule( :predicate_name => simple(:predicate_name)) { predicate_name }
  rule( :negation => simple(:negation) , :predicate_name => simple(:predicate_name)) { "not #{predicate_name}" }
  rule( :predicate_fragment => simple(:predicate_fragment) , :predicate_end => simple(:predicate_end)) {
   	"#{predicate_fragment}_#{predicate_end}"
  }
  rule( :negation => simple(:negation) , :predicate_fragment => simple(:predicate_fragment) , :predicate_end => simple(:predicate_end)) {
   	"not #{predicate_fragment}_#{predicate_end}"
  }
  rule( :quantifier => simple(:quantifier)) { quantifier }
  rule( :predicate  => simple(:p) , :quantifier => simple(:q) , :class_expression => simple(:c)) {"#{p} #{q} #{c}"}
  rule( :pos_class => simple(:c) , :qualifier => simple(:q)) { "( #{c} and #{q} )" }

  rule( :qualified_class => simple(:c) ) { c }
  rule( :disjunction => sequence(:c)) { "( " + c.inject { |list, elt| "#{list} or #{elt}" } + " )" }
  rule( :conjunction => sequence(:c)) { "( " + c.inject { |list, elt| "#{list} and #{elt}" } + " )" }

  rule( :class_expression => simple(:class_expression)) { class_expression }


  ###################### sentences ######################
  
  ######################  Definitions and propostions

  #foo is defined as bar
  rule( :class_name => simple(:c) , :defined => simple(:d)) { "Class: #{c} EquivalentTo: #{d}" }
  
  #Doobie_Brothers is enumerated as John and Bob
  rule( :class_name => simple(:c) , :enumerated => sequence(:enum)) {
   "Class: #{c} EquivalentTo: { " + enum.inject { |list, elt| "#{list} , #{elt}" } + " }" }

  #every dog is a mammal
  rule( :every => simple(:every) , :class_name => simple(:c) , :class_expression => simple(:exp)) {
  	"Class: #{c} SubClassOf: #{exp}"  }
  #dog is a subclass of mammal
  rule( :class_name => simple(:c) , :subclass_of => simple(:subclass_of) , :class_expression => simple(:exp)) {
  	"Class: #{c} SubClassOf: #{exp}"  }

  #no cat is a dog
  rule(:negation => simple(:neg) , :class_name => simple(:c), :class_expression => simple(:exp)) {
  	"Class: #{c} DisjointWith: #{exp}"}

  #Dog and Cat and Bird are disjoint
  rule(:disjoint => sequence(:disjoint)) {
  	"DisjointClasses: " +disjoint.inject { |list, elt| "#{list} , #{elt}" } }

  #some dogs are cute
  rule( :some => simple(:some) , :class_name => simple(:c) , :class_expression => simple(:exp)) {
  	"Individual: #{nameGenerator.getNewIndivName(c,exp,true)} Types: #{c} , #{exp}"  }
  
  #some dogs aren't cute
  rule( :some => simple(:some) , :class_name => simple(:c) , 
  		:negation => simple(:negation) , :class_expression => simple(:exp)) { 
  	"Individual: #{nameGenerator.getNewIndivName(c,exp,false)} Types: #{c} , Not #{exp}"  }

  ######################  FACTS

  #John is an idiot
  rule( :indiv_name => simple(:i) , :class_expression => simple(:y)) { "Individual: #{i} Types: #{y}" }

  #John is not an idiot
  rule( :indiv_name => simple(:i) , :negation => simple(:negation) , :class_expression => simple(:y)) {
  	"Individual: #{i} Types: not #{y}" }

  #John doesn't love Jane
  rule( :indiv1 => simple(:i1) , :predicate => simple(:p), :indiv2 => simple(:i2)) { "Individual: #{i1} #{p} #{i2} " }

  #John is the same as Jill
  rule( :indiv1 => simple(:i1) , :indiv2 => simple(:i2)) { "SameIndividual: #{i1} , #{i2} " }

  #Jill and Mary and Robert are different
  rule(:different => sequence(:i)) { "DifferentIndividuals: " + i.inject { |list, elt| "#{list} , #{elt}" } } 

  # %OWL
  # """
  #   Class: Nut 
  #   Class: Peanut 
  #       SubClassOf: Nut   
  # """

  ######################  Leaf
  rule( :indiv_name => simple(:i)) { i }

  ######################  Intermediary nodes
  rule( :universal_negative => simple(:u)) { u }
  rule( :universal_positive => simple(:u)) { u }
  rule( :particular => simple(:particular)) { particular }
  rule( :instance => simple(:i)) { i }
  rule( :relation => simple(:r)) { r }
  rule( :equation => simple(:e)) { e }
  rule( :proposition => simple(:p)) { p }
  rule( :fact => simple(:f)) { f }
  rule( :source => simple(:source)) { source }
  rule( :definition => simple(:d)) { d }
  rule( :sentence => simple(:s)) { s }

  ######################  Document root
  rule( :text => sequence(:c)) { c.inject { |text, sentence| "#{text} \r\n #{sentence}" } }
  
end


  def parse(str)
    SconeParser = SconeParser.new

    puts "=============================="
    puts str
  
    interm_tree = SconeParser.parse_with_debug(str)

	puts "Intermediary tree : \n"
    puts interm_tree

    transf = SconeParseTransform.new

   ast = transf.apply(interm_tree)

    puts "AST : \n"
    pp ast

    rescue Parslet::ParseFailed => failure
    #puts failure.cause.ascii_tree

    puts "=============================="
  end


  # #### Things to look at 
  # parse "neither a dumbass or a genius or a crook" # should fail but succeed

  #facts
  parse "John is an idiot" #instance
  parse "John is not an idiot" #instance negation
  parse "Jill and Mary and Robert are different" #different
  parse "John is the same as Jill" #equation
  parse "John doesn't love Jane" #relation  Bug:  Individual: John Facts: not love Jane

  parse "foo is defined as bar" #definition defined
  parse "Doobie_Brothers is enumerated as John and Bob" #definition defined

  parse "some dogs are cute" # particular => deal with uppercase
  parse "some dogs aren't cute"
  parse "no cat is a dog" # universal negative 1
  parse "Dog and Cat and Bird are disjoint" # universal negative 2
  parse "every dog is a mammal"
  parse "dog is a subclass of mammal"

  parse '
  %OWL
  """
    Class: Nut 
    Class: Peanut 
        SubClassOf: Nut   
  """
    '

  parse "John is a dumbass" #pos_class
  parse "John is not an idiot" #class_atom 
  
  # #qualified_class
  parse "John is  an idiot who is a dumbass" #qualified_class : qualifier : is class atom
  parse "John is  an idiot, who is a dumbass" #qualified_class qualifier : is class atom

  parse "John is an idiot that does love some cats" #qualified_class qualifier : predicate2
  parse "John is an idiot, that doesn't love some cats" #qualified_class
  parse "John is an idiot, that does not love some cats" #qualified_class
  parse "John is an idiot, that doesn’t love some cats" #qualified_class
  
  parse "John is an idiot, that has as love some cats" #qualified_class : predicate2
  parse "John is an idiot, that has not as love only cats" #qualified_class
  parse "John is an idiot, that hasn’t as love some cats" #qualified_class
  parse "John is an idiot, that hasn't as love some cats" #qualified_class

  parse "John is a mammal, who is located on some tree" #qualified_class : predicate1
  parse "John is a mammal, who is not located on some tree"
  parse "John is a mammal, who is a father of some cat"
   parse "John is a mammal, who is the father of some cat" # => error when using the !

  # conjunctions and disjunctions
  parse "John is an idiot, who is a dumbass or a genius or a crook" 
  parse "John is an idiot, who is a dumbass and a genius and a crook"
  parse "John is an idiot, who is neither a dumbass nor a genius nor a crook"
  parse "John is a dumbass and a genius and a crook" 
  parse "John is a dumbass or a genius or a crook"
  parse "John is neither a dumbass nor a genius nor a crook" 

  # #several lines
  parse "John is an idiot
    		John is   an dumbass" #pos_class
        
        
        #### Fabian
        
                
  parse "John is an idiot, who is a dumbass, who is a genius"
  parse "John is an idiot who does love some cats"
  parse "Mother is defined as not father"
  parse "Mother is defined as parent and not father"
  parse "Mother is defined as a woman who does have some child"
  parse "Mother is defined as a woman who is parent of some child."
